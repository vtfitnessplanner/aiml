# Gobbleup ML code

## Introduction
This repository holds all the ML code necessary to create informed clustering decisions on our data. Gobbleup will be using Sentence-BERT for semantically similar embeddings and unsuperivsed clustering for grouping food items.

## Clustering Approach
The clustering algorithm we decided to implement is a BERT enhanced unsupervised clustering model called BERTopic (https://arxiv.org/pdf/2203.05794.pdf). Below is a diagram depicting the architecture of BERTopic:

![BERTopic](https://d3i71xaburhd42.cloudfront.net/bc99e49e0b4d18f802f1d64b6d6e465082c1acae/3-Figure1-1.png)

## LLM
To enhance our BERTopic predictions, we hosted a LLaMa-7B model on our Kubernetes cluster with a goal to pass in topic information and produce a "summary" of the clusters. This summary can help us make personalized meal plans dynamically and automatically. Hosting implementation was taken from: https://github.com/chenhunghan/ialacol. Though we experience response time issues due to lack of GPU resources, here is an example on how to call our model (this will take 30 seconds ish):
```
curl -X POST \
     -H 'Content-Type: application/json' \
     -d '{ "messages": [{"role": "user", "content": "How are you?"}], "model": "llama-2-7b-chat.ggmlv3.q4_0.bin", "stream": false}' \
     https://llama-gobbleup.discovery.cs.vt.edu/v1/chat/completions
```

### LLM Installation process
Make a `values.yaml` file:
```
replicas: 1
deployment:
  image: ghcr.io/chenhunghan/ialacol-metal:latest
  env:
    DEFAULT_MODEL_HG_REPO_ID: TheBloke/Llama-2-7B-Chat-GGML
    DEFAULT_MODEL_FILE: llama-2-7b-chat.ggmlv3.q4_0.bin
resources:
  requests:
    cpu: 12
    memory: 16Gi
  limits:
    cpu: 12
    memory: 16Gi
model:
  persistence:
    size: 5Gi
    accessModes:
      - ReadWriteOnce
service:
  type: ClusterIP
  port: 8000
  annotations: {}
nodeSelector: {}
tolerations: []
affinity: {}
```

Apply the Kubernetes resources with values configuration:
```
helm repo add ialacol https://chenhunghan.github.io/ialacol
helm repo update
helm install llama2-7b-chat ialacol/ialacol -f values.yaml
kubectl apply -f ./kubernetes/llama-ingress.yaml
```

## Codebase
This codebase trains a BERTopic model on all the data in our Food table for each date present in the FoodDate table, saving each model to our Minio instance. Below is our hosting overview/networking of components:

![CapstoneAIoverview](AI-process-bertopic-casptone.PNG)

Note: in this diagram, this codebase strictly deals with the "BERTopic Train Pod" and "Cronjob". For more information on our "AI Flask Backend", please go to the AIML-Backend repo in our project.

Model storage is accessible via our Minio Storage service: https://minio-gobbleup.discovery.cs.vt.edu/browser/model-storage/

## References
* https://www.sbert.net/examples/applications/clustering/README.html
* https://arxiv.org/pdf/1908.10084.pdf
