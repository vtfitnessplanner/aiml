import mysql.connector
import pandas as pd
import numpy as np
from minio import S3Error, Minio
from bertopic import BERTopic
import os
import json
'''
DEVELOPMENT VERSION
Python code that saves a BERTopic model for each day in the FoodDate table.
'''
def get_data():
    '''
    Returns a pandas dataframe of the Gobbleup food table.
    '''
    # password = os.environ.get('MYSQL_ROOT_PASSWORD')
    dataframes = []

    password = "password"

    db_connection = mysql.connector.connect(
        host="127.0.0.1",
        user="root",
        password=password,
        database="vtmealplandb",
        connection_timeout=10    
    )

    if db_connection.is_connected():
        db_info = db_connection.get_server_info()
        print("Connected to Database! Version: ", db_info)
    
    cursor = db_connection.cursor()

    food_dates = "SELECT DISTINCT foodDateDate FROM FoodDate;"
    cursor.execute(food_dates)

    food_dates_list = [result[0] for result in cursor.fetchall()]
    print("Food Dates: ", food_dates_list)

    for date in food_dates_list:
        get_food_stmt = """SELECT Food.foodName FROM Food INNER JOIN FoodDate on Food.foodID = FoodDate.foodID WHERE FoodDate.foodDateDate = %s;"""

        cursor.execute(get_food_stmt, (date,))
        records = cursor.fetchall()
        df = pd.DataFrame(records, columns=cursor.column_names)
        dataframes.append(df)
        # df.to_csv("./data.csv")
    

    return dataframes, food_dates_list

def save_to_minio(model_dir_path):
    '''
    Saves model data to Minio instance on cluster.
    '''
    access_key = "grizzlord-dev"
    secret_key = "grizzlord-dev-password"
    endpoint = "localhost:9000"
    bucket_name = "model-storage"
    minio_client = Minio(endpoint, access_key=access_key, secret_key=secret_key, secure=False)

    found = minio_client.bucket_exists(bucket_name)
    if not found:
        minio_client.make_bucket(bucket_name)
    else:
        print(f"Bucket '{bucket_name}' already exists.")

    try:
        upload_directory(minio_client, bucket_name, model_dir_path)
    except S3Error as exc:
        print("Error occurred.", exc)

def upload_directory(minio_client, bucket_name, model_dir_path):
    '''
    Helper function to upload directory to bucket.
    '''
    # Walk through the local directory
    for root, dirs, files in os.walk(model_dir_path):
        for file in files:
            local_file_path = os.path.join(root, file)
            object_name = os.path.relpath(local_file_path, start=os.path.dirname(model_dir_path))
            # Upload the file to MinIO bucket
            try:
                minio_client.fput_object(bucket_name, object_name, local_file_path)
                print(f"File {local_file_path} uploaded as {object_name}")
            except S3Error as exc:
                print(f"Failed to upload {local_file_path}. Error: {exc}")


def main1():
    dataframes, food_dates_list = get_data()

    # trains and saves the models
    for i, df in enumerate(dataframes):
        model = BERTopic()
        food_date = food_dates_list[i]
        food_data = df["foodName"].tolist()
        save_dir = f"../aiml-backend/saved_models/model-{food_date}"
        
        print(f"DF_{food_date}: {len(df)} entries!")

        topics, probs = model.fit_transform(food_data)
        model.reduce_topics(food_data, nr_topics=30)

        embedding_model = "sentence-transformers/all-MiniLM-L6-v2"
        model.save(f"{save_dir}", serialization="pytorch", save_ctfidf=True, save_embedding_model=embedding_model)
        
        with open(f'{save_dir}/rep_docs.json', 'w') as filehandle:
            json.dump(model.get_representative_docs(), filehandle)
        
        print(f"Trained DF_{food_date} and saved to {save_dir}")

def main2():
    dataframes, food_dates_list = get_data()

    # trains and saves the models
    for i, df in enumerate(dataframes):
        model = BERTopic()
        food_date = food_dates_list[i]
        food_data = df["foodName"].tolist()
        save_dir = f"./data/saved_models/model-{food_date}"
        
        print(f"DF_{food_date}: {len(df)} entries!")

        topics, probs = model.fit_transform(food_data)
        model.reduce_topics(food_data, nr_topics=30)

        embedding_model = "sentence-transformers/all-MiniLM-L6-v2"
        model.save(f"{save_dir}", serialization="pytorch", save_ctfidf=True, save_embedding_model=embedding_model)

        with open(f'{save_dir}/rep_docs.json', 'w') as filehandle:
            json.dump(model.get_representative_docs(), filehandle)

        print(f"Trained DF_{food_date} and saved to {save_dir}")
        save_to_minio(save_dir)

if __name__ == "__main__":
    main2()
