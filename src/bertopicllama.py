import numpy as np
import pandas as pd
import json
import requests

class LlamaQuery:
    '''
    Class that queries the LLaMa model on the gobbleup namespace.
    '''
    def __init__(self, options=None):

        if options is None:
            self.options = {
                "A": "Coffee",
                "B": "Protein",
                "C": "Drink",
                "D": "Carb Heavy",
                "E": "Seafood",
                "F": "Fast Food",
                "G": "Sugary"
            }
        else:
            self.options = options

        self.url = 'https://llama-gobbleup.discovery.cs.vt.edu/v1/chat/completions'
        
    def query_llama(self, topic_data):
        '''
        Queries the llama model with the given topic data.
        '''
        headers = {'Content-Type': 'application/json'}

        prompt = f"""Given this cluster topic: {topic_data}, choose the best category:"""
        for choice in self.options:
            prompt += f"\n{choice}. {self.options[choice]}"

        prompt += "\nYou must respond with ONLY the letter associated with the correct option (A-G). NOTHING MORE.\n"

        print(prompt)
        # Define the data payload for POST request
        data = {
            "messages": [
                {"role": "user", "content": prompt}
            ],
            "model": "llama-2-7b-chat.ggmlv3.q4_0.bin",
            "stream": False,
            "temperature": "0.6",
            "top_p": "1",
            "top_k": "0",
            "max_tokens": "30"
        }

        # Make the POST request
        response = requests.post(self.url, json=data, headers=headers)

        # Check if the request was successful
        if response.status_code == 200:
            # Get JSON data from the response
            response_data = response.json()
            content = response_data['choices'][0]['message']['content']
            print("Received response:", content)
            return content
            # Example: Store response data in a variable or process it
            # You can write it to a file, database, or any storage as needed
        else:
            print("Failed to receive valid response:", response.status_code)
    

    def get_options(self):
        return self.options
    
# if __name__ == "__main__":
#     lq = LlamaQuery()
#     response_topic_one = lq.query_llama("[caramel, macchiato, iced, soy, short, grande]")

#     response_topic_two = lq.query_llama("[wings, bonein, boneless, buffalo, carolina]")