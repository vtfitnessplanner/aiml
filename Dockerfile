FROM python:3.12.2-slim

WORKDIR /usr/src/app 

COPY . .
RUN apt-get update && apt-get install gcc -y
RUN pip3 install torch==2.2.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
RUN pip install -r requirements.txt
RUN pip install bertopic --no-deps

EXPOSE 80

CMD ["python", "./src/train_and_save.py"]